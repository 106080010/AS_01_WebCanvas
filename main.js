
var canvas = document.getElementById('canvasInterface');
var ctx = canvas.getContext('2d');
var inC = document.getElementById("incolor");
var painting = document.getElementById('Canvas');
var paint_style = getComputedStyle(painting);
canvas.width = parseInt(paint_style.getPropertyValue('width'));
canvas.height = parseInt(paint_style.getPropertyValue('height'));

var Shapes = null;
var refShape = ['sq', 'tri', 'c'];
var textblock;
var draw = true;
var outline = false;
var status = null;
var erase = 0;
var cArray = [];
var cStep = -1;
var brushState = 5;
var mouse = {x: 0, y: 0};
var textpos = {x: 0, y: 0};
var FONT = 'Arial';
ctx.lineWidth = 5;
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.strokeStyle = '#7fcb11';

$("#textblock").keypress(function (key) {
    console.log("kepress : " + key.keyCode);
    var text = document.getElementById('textinput');
    var size = document.getElementById('myRange');
    if (key.keyCode == 13 && text) { // enter
        ctx.font = size.value + "px " + FONT;
        ctx.fillText(text.value, textpos.x, textpos.y);
        cPush();
    }
});

function FillShape(s) {
    outline = true;
    if(Shapes != null && s != Shapes){
        for (let i = 0; i < 3 ; i++){
            if(refShape[i] == s){
                let shape_id = document.getElementById(s);
                shape_id.style.backgroundColor = "#ffffff";
                shape_id.style.color = "#000000";
                let shape2_id = document.getElementById(Shapes);
                shape2_id.style.backgroundColor = "#000000";
                shape2_id.style.color = "#ffffff";
                Shapes = s;
            }
        }
    } else if(Shapes == null) {
        Shapes = s;
        let shape_id = document.getElementById(s);
        shape_id.style.backgroundColor = "#ffffff";
        shape_id.style.color = "#000000";
    } else if(s == Shapes){
        let shape_id = document.getElementById(s);
        shape_id.style.backgroundColor = "#000000";
        shape_id.style.color = "#ffffff";
        outline = false;
        Shapes = null;
    }
    console.log(Shapes);
}

function ChFont(font) {
    FONT = font;
    var textfont = document.getElementById('textfont');
    textfont.style.fontFamily = font;
}

function textsize() {
    var outface = document.getElementById('div_range');
    var text = document.getElementById('myRange');
    outface.innerText = text.value + ' px';
}

function TypeText() {
    textblock = document.getElementById('textblock');
    var typeface = document.getElementById('typein');
    if(draw) {
        typeface.style.backgroundColor = '#ffffff';
        typeface.style.color = '#000000';
        textblock.innerHTML = "<input id='textinput' type=\"text\" value=\"hello\" style='border-style: dashed; border-color: rgba(0,0,0,0.5); background-color: rgba(0,0,0,0); text-align: center'>\n" ;
        draw = false;
    } else {
        typeface.style.backgroundColor = '#000000';
        typeface.style.color = '#ffffff';
        textblock.innerHTML = "";
        draw = true;
    }
}

function cPush() {
    cStep++;
    if (cStep < cArray.length) { cArray.length = cStep; console.log('Done');}
    cArray.push(ctx.getImageData(0,0, canvas.width, canvas.height));
    console.log(cArray);
    console.log('Push Step '+cStep);
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        ctx.putImageData(cArray[cStep], 0, 0);
    } else if (cStep == 0) {
        ctx.clearRect(0,0,canvas.width,canvas.height);
        cStep--;
    }
    console.log('Pop Step '+cStep);
}

function cRedo() {
    if (cStep < cArray.length-1) {
        cStep++;
        ctx.putImageData(cArray[cStep], 0, 0);
    }
    console.log('Push Step '+cStep);
}

document.getElementById('imgUp').onchange = function() {
    var img = new Image();
    img.src = URL.createObjectURL(this.files[0]);
    img.onload = function draw() {
        img.width = canvas.width;
        img.height = canvas.height;
        ctx.drawImage(img, 0,0);
        cPush();
        console.log('Paint ' + img.src);
    };
};

function downloadImg() {
    var button_dload = document.getElementById('dloadImg');
    button_dload.href = canvas.toDataURL('image/png');
}

function BrushSize (size){
    ctx.lineWidth = size;
    if(size != brushState) {
        for (let i = 5; i <= 20; i *= 2) {
            if (size == i) {
                let brush_id = document.getElementById('b' + size);
                brush_id.style.backgroundColor = "#eff4ff";
                brush_id.style.color = "#000000";
            }
        }
        for (let i = 5; i <= 20; i *= 2) {
            if (brushState == i) {
                let brush_id = document.getElementById('b' + brushState);
                brush_id.style.backgroundColor = "#000000";
                brush_id.style.color = "#ffffff";
            }
        }
        brushState = size;
    }
}

function Erase() {
    var element = document.getElementById("eraser");
    if (erase == 0) {
        ctx.globalCompositeOperation = "destination-out";
        element.style.backgroundColor = "#eff4ff";
        element.style.color = "#000000";
        canvas.style.cursor = "url(\"https://img.icons8.com/ios/30/000000/eraser-filled.png\") 5 25, auto";//eraser
        erase = 1;
    } else {
        ctx.globalCompositeOperation = "source-over";
        element.style.backgroundColor = "#000000";
        element.style.color = "#ffffff";
        canvas.style.cursor = "url(\"https://img.icons8.com/ios/30/000000/edit.png\") 0 30, auto"; //pencil
        erase = 0;
    }
}

function Selector(e) {
    if(status == null){
        status = e.id;
        if(status == 'eraser'){
            Erase();
        } else if(status == 'typein'){
            TypeText();
        } else if(status == 'sq' || status == 'tri' || status == 'c'){
            FillShape(status);
        }
    } else if(status != null && e.id != status){
        let i = 2;
        while (i) {
            if (status == 'eraser') {
                Erase();
            } else if (status == 'typein') {
                TypeText();
            } else if (status == 'sq' || status == 'tri' || status == 'c') {
                FillShape(status);
            }
            status = e.id;
            i--;
        }
    } else if(e.id == status){
        if(status == 'eraser'){
            Erase();
        } else if(status == 'typein'){
            TypeText();
        } else if(status == 'sq' || status == 'tri' || status == 'c'){
            FillShape(status);
        }
        status = null;
    }
}

function ColorP (){
    var c = document.getElementById("COLOR");
    c.style.color = inC.value;
    ctx.strokeStyle = inC.value;
}

var Painting = function() {
    ctx.lineTo(mouse.x, mouse.y);
    ctx.stroke();
};

var origin_pos = {x:0,y:0};
var origin_canvas;
var start = false;
var points = [];

canvas.addEventListener('mousemove', function (e) {
    mouse.x = e.pageX - this.offsetLeft;
    mouse.y = e.pageY - this.offsetTop;
    if(start){
        ctx.putImageData(origin_canvas, 0, 0);
        if(Shapes == 'sq') {
            ctx.strokeRect(origin_pos.x, origin_pos.y, mouse.x - origin_pos.x, mouse.y - origin_pos.y);
        } else if(Shapes == 'c'){
            var radius = Math.sqrt(Math.pow(mouse.x - origin_pos.x,2) + Math.pow(mouse.y - origin_pos.y,2));
            ctx.beginPath();
            ctx.arc(origin_pos.x, origin_pos.y, radius, 0, 2 * Math.PI);
            ctx.stroke();
        } else {
            ctx.beginPath();
            ctx.moveTo(origin_pos.x, origin_pos.y);
            ctx.lineTo(origin_pos.x,mouse.y);
            ctx.lineTo(mouse.x,mouse.y);
            ctx.lineTo(origin_pos.x,origin_pos.y);
            ctx.stroke();
        }
    }
}, false);

canvas.addEventListener('mousedown', function (e) {
    ctx.beginPath();
    ctx.moveTo(mouse.x, mouse.y);

    origin_pos.x = mouse.x;
    origin_pos.y = mouse.y;

    if(outline){
        start = true;
        origin_canvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
        if(Shapes == 'sq') {
            ctx.rect(origin_pos.x, origin_pos.y, mouse.x - origin_pos.x, mouse.y - origin_pos.y);
            ctx.stroke();
        } else if(Shapes == 'c'){
            var radius = Math.sqrt(Math.pow(mouse.x - origin_pos.x,2) + Math.pow(mouse.y - origin_pos.y,2));
            ctx.arc(origin_pos.x, origin_pos.y, radius, 0, 2 * Math.PI);
            ctx.stroke();
        } else {
            ctx.lineTo(origin_pos.x,mouse.y);
            ctx.lineTo(mouse.x,mouse.y);
            ctx.lineTo(origin_pos.x,origin_pos.y);
            ctx.stroke();
        }
    } else if(draw) {
        canvas.addEventListener('mousemove', Painting, false);
    } else {
        textblock.style.left = e.pageX + 'px';
        textblock.style.top = e.pageY + 'px';
        textpos.x = mouse.x;
        textpos.y = mouse.y;
    }
}, false);

canvas.addEventListener('mouseup', function () {
    if(start){
        ctx.putImageData(origin_canvas,0,0);
        if(Shapes == 'sq') {
            ctx.rect(origin_pos.x, origin_pos.y, mouse.x - origin_pos.x, mouse.y - origin_pos.y);
            ctx.stroke();
        } else if(Shapes == 'c'){
            var radius = Math.sqrt(Math.pow(mouse.x - origin_pos.x,2) + Math.pow(mouse.y - origin_pos.y,2));
            ctx.beginPath();
            ctx.arc(origin_pos.x, origin_pos.y, radius, 0, 2 * Math.PI);
            ctx.stroke();
        } else {
            ctx.beginPath();
            ctx.moveTo(origin_pos.x, origin_pos.y);
            ctx.lineTo(origin_pos.x,mouse.y);
            ctx.lineTo(mouse.x,mouse.y);
            ctx.lineTo(origin_pos.x,origin_pos.y);
            ctx.stroke();
        }
        cPush();
        start = false;
    }
    else if(draw) {
        cPush();
        canvas.removeEventListener('mousemove', Painting, false);
    }
}, false);

function clean() {
    cStep = -1;
    cArray = [];
    var Img = document.getElementById('imgUp');
    Img.value = [];
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        console.log("Clean");
        ctx.clearRect(0,0,canvas.width,canvas.height);
    } else {
        console.log("Something Wrong");
    }
}