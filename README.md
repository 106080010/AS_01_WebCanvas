# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* Brush and eraser
    * combined with several functions
        * canvas.addEventListener('mouseup', function () { ...
            1. To call "canvas.removeEventListener('mousemove', Painting, false);".
        * canvas.addEventListener('mousedown', function (e) { ...
            1. Check whether the cursor is on the canvas and then begin drawing.
            2. To call "canvas.addEventListener('mousemove', Painting, false);".
        * canvas.addEventListener('mousemove', function (e) { ...
            1. Draw a picture.
        * function Painting
            1. Draw a line.
        * function Eraser
            1. Erase a line, by changing ctx.globalCompositeOperation.

* Color selector
    * function ColorP
        1. By changing ctx.strokeStyle to change brush color.

* Simple menu
    * function BrushSize (size)
        1. By changing ctx.lineWidth to change brush size. 
        2. Use var brushState to record the current size, that can handle the choosing conflict.

* Typing text input
    * combined with several functions
        * $("#textblock").keypress(function (key)
            1. To get the input text and then draw text on the canvas.
        * function TypeText
            1. To create the input text block on the mousedown event position.

* Font menu
    * combined with several functions
        * function ChFont(font)
            1.  Change (var).style.fontFamily.
        * function textsize
            1. Change the value in the input, id = 'myRange'.
            
* Cursor icon
    * The changing method is done by Erase() , to changing between pencil and eraser.
    
* Refresh button
    * function clean
        1. To call ctx.clearRect, clear cArray, and set cStep to origin.
        
* Different brush shapes
    * They are all assembled in Drawing functions.
    * Circle is by ctx.arc, Rectangle is by ctx.rect, Triangle is by three ctx.lintTo func.
    * Use var Shapes to detect which should be drawed.
    
* Un/Re-do button
    * combined with several functions
        * cPush
            1. Use an array to save the certain step canvas.
            2. Use cStep to record the current step. 
        * cUndo
            1. Redraw the previous step canvas by ctx.putImageData.
        * cRedo
            1. Redraw the previous step canvas by ctx.putImageData.
            
* Up/Download button
    * function downloadImg
        1. The core code : canvas.toDataURL.
        2. Use < a > and attribute download.
    * document.getElementById('imgUp').onchange = function
        1. The core code:
            * img.src = URL.createObjectURL(this.files[0]);
            * img.onload = function draw
            
* Some other function:
    * Selector(e)
        1. To prevent typing text, draw picture, draw/erase in the same time
    * FillShape
        1. To change the selected icon color, and save the current Shapes.
    
    

